import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <div className="app-header jumbotron">
          <img src={logo} className="app-logo" alt="logo" />
          <h1>Lista de Contactos</h1>
        </div>
        <p className="app-intro container">
          Para empezar, edita el archivo <code>src/App.js</code>.
        </p>
      </div>
    );
  }
}

export default App;
